import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { LayoutComponent } from "./components/layout/layout.component";
import { ContainerComponent } from "./components/pages/container/components/container.component";
import { DetailsComponent } from "./components/pages/policies/components/details/details.component";
import { ViewComponent } from "./components/pages/policies/components/view/view.component";
import { SettingsHelpComponent } from "./components/pages/settings/components/help/help.component";
import { ConcentratorAccountComponent } from "./components/pages/support/components/accounts/concentratoraccount.component";
import { ViewDocumentComponent } from "./components/pages/support/components/view-document/view-document.component";

import { AuthGuard } from "./guard/auth.guard";
import { PrivacityComponent } from "./shared/components/privacity/privacity.component";
import { SettingsSecurityComponent } from './components/pages/settings/components/security/security.component';
import { SecurityPasswordComponent } from "./components/pages/settings/components/password/password.component";
import { GeneralConditionsComponent } from "./components/pages/support/components/general-conditions/general-conditions.component";
import { HelpPrivacityComponent } from "./components/pages/settings/components/privacity/privacity.component";
import { HelpAppInfoComponent } from "./components/pages/settings/components/app-info/app-info.component";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: '/container',
                pathMatch: 'full'
            },
            {
                path: 'container',
                canActivate: [AuthGuard],
                component: ContainerComponent,
                children: [
                    {
                        path: '',
                        loadChildren: () => import('~/app/components/pages/pages.module').then(m => m.PagesModule)
                    }
                ]
            },
            {
                path: 'policies/view/:file',
                canActivate: [AuthGuard],
                component: ViewComponent
            },
            {
                path: 'policies/details/:itempolicie',
                canActivate: [AuthGuard],
                component: DetailsComponent
            },
            {
                path: 'login',
                loadChildren: () => import('~/app/components/login/login.module').then((m) => m.LoginModule)
            },
            {
                path: 'privacity',
                canActivate: [AuthGuard],
                component: PrivacityComponent
            },
            {
                path: 'support/concentrator-account',
                canActivate: [AuthGuard],
                component: ConcentratorAccountComponent
            },
            {
                path: 'support/concentrator-account/view-document/:documentname',
                canActivate: [AuthGuard],
                component: ViewDocumentComponent
            },
            {
                path: 'support/general-conditions',
                canActivate: [AuthGuard],
                component: GeneralConditionsComponent
            },
            {
                path: 'settings/help',
                canActivate: [AuthGuard],
                component: SettingsHelpComponent
            },
            {
                path: 'settings/help/privacity',
                canActivate: [AuthGuard],
                component: HelpPrivacityComponent
            },
            {
                path: 'settings/help/info',
                canActivate: [AuthGuard],
                component: HelpAppInfoComponent
            },
            {
                path: 'settings/security',
                canActivate: [AuthGuard],
                component: SettingsSecurityComponent
            },
            {
                path: 'settings/security/password',
                canActivate: [AuthGuard],
                component: SecurityPasswordComponent
            },
            {
                path: 'cotizador',
                loadChildren: () => import('~/app/components/cotizador/cotizador.module').then(m => m.CotizadorModule)
            },
            {
                path: 'recovery-password',
                loadChildren: () => import('~/app/components/password-recovery/password-recovery.module').then(m => m.PasswordRecoveryModule)
            }
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
