import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { FloatingButtonComponent } from "./components/floating-button/floating-button.component";
import { LoaderComponent } from "./components/loader/loader.component";
import { PrivacityComponent } from "./components/privacity/privacity.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        LoaderComponent,
        FloatingButtonComponent,
        PrivacityComponent
    ],
    declarations: [
        LoaderComponent,
        FloatingButtonComponent,
        PrivacityComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
