import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';
import { environment } from '~/environment/environment';

@Injectable({ providedIn: 'root' })
export class PoliciesService {

    constructor(
        private http: HttpClient
    ) { }

    getPolicies(clientID: number): Observable<Array<PolicieModel>> {
        return this.http.get<Array<PolicieModel>>(`${environment.apiMark}/v1/app-cliente/get-registro-cliente/${clientID}`);
    }

    downLoadPolicie(filePolicie: string, headers: any) {
        return this.http.get(`${environment.apiMark}/auth-cliente/poliza/${filePolicie}`, {
            responseType: 'blob',
            headers: headers
        });
    }

}