import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core';

@Component({
    selector: 'app-official-identification',
    templateUrl: 'official-identification.component.html',
    styleUrls: ['official-identification.component.scss']
})

export class OfficialIdentificationComponent implements OnInit {
    
    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() { }
}