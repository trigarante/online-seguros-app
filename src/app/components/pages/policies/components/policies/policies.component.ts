import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, isAndroid, isIOS, Page } from '@nativescript/core';
import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';
import { PoliciesService } from '~/app/core/Services/Policies/policies.service';

@Component({
    selector: 'app-policies',
    templateUrl: 'policies.component.html',
    styleUrls: ['policies.component.scss']
})

export class PoliciesComponent implements OnInit {

    dataLoaded: boolean = false;
    textLoader: string = 'Cargando pólizas\nEspere un momento, por favor.';
    userActive: number;
    policiesArr: Array<PolicieModel> = [];

    isIOS: boolean;
    isAndroid: boolean;

    constructor(
        private routerExtensions: RouterExtensions,
        private policiesService: PoliciesService,
        private page: Page
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.page.actionBarHidden = true;
        this.getPolicies();
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
    }

    ngOnInit() { }

    getPolicies() {
        this.policiesService.getPolicies(this.userActive)
            // tslint:disable-next-line: deprecation
            .subscribe((data: Array<PolicieModel>) => {
                this.policiesArr = data;
                for (let index = 0; index < this.policiesArr.length; index++) {
                    const fechaSplit = this.policiesArr[index].fechaInicio.split('-');
                    this.policiesArr[index].fechaInicio = fechaSplit[2] + ' ' + this.getMonthName(Number(fechaSplit[1])) + ' ' + fechaSplit[0];
                    this.policiesArr[index].fechaFin = fechaSplit[2] + ' ' + this.getMonthName(Number(fechaSplit[1])) + ' ' + (Number(fechaSplit[0]) + 1).toString();
                    this.policiesArr[index].datos = JSON.parse(data[index].datos);
                }
            }, error => {
                console.log(error.message);
            }, () => {
                this.dataLoaded = true;
            });
    }

    private getMonthName(numberMonth: number) {
        const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        return months[numberMonth - 1];
    }

    onViewTap(file: string, policeNumer: string, registerID: number) {
        // tslint:disable-next-line: object-literal-shorthand
        const paramsArr = JSON.stringify({ file: file, policieNumber: policeNumer, registerID: registerID });
        this.routerExtensions.navigate(['/policies/view', paramsArr], {
            transition: {
                name: 'fade'
            }
        });
    }

    onPetitionsTap(anything: any) {
        this.routerExtensions.navigate(['/policies/petitions'], {
            transition: {
                name: 'fade'
            }
        });
    }

    onDetailsTap(itemPolicie: PolicieModel) {
        /* itemPolicie.datos = JSON.stringify(itemPolicie.datos);
        this.routerExtensions.navigate(['/policies/details', JSON.stringify(itemPolicie)], {
            transition: {
                name: 'fade'
            }
        });
        itemPolicie.datos = JSON.parse(itemPolicie.datos); */
        alert('El componente aún se encuentra en desarrollo');
    }

    selectImage(alias: string) {
        const url: string = `~/assets/iconos-aseguradoras`;
        const images = [
            { alias: "ABA", name: "aba.svg" },
            { alias: "SEGUROS AFIRME", name: "afirme.svg" },
            { alias: "ANA SEGUROS", name: "ana.svg" },
            { alias: "AXA", name: "axa.svg" },
            { alias: "BANORTE", name: "banorte.svg" },
            { alias: "GENERAL DE SEGUROS", name: "general.svg" },
            { alias: "GNP", name: "gnp.svg" },
            { alias: "HDI", name: "hdi.svg" },
            { alias: "INBURSA", name: "inbursa.svg" },
            { alias: "MAPFRE", name: "mapfre.svg" },
            { alias: "MIGO", name: "migo.svg" },
            { alias: "EL POTOSI", name: "elpotosi.svg" },
            { alias: "QUALITAS", name: "qualitas.svg" },
            { alias: "ZURA", name: "sura.svg" },
            { alias: "ZURICH", name: "zurich.svg" },
            { alias: "EL AGUILA", name: "elaguila.svg" },
            { alias: "AIG", name: "aig.svg" },
            { alias: "LA LATINO", name: "lalatino.svg" },
        ];

        return `${url}/${images.find((e) => e.alias === alias).name}`;
    }

    selectSize(alias: string, typeSize: string) {
        const sizes = [
            {
                alias: "ABA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "SEGUROS AFIRME",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "ANA SEGUROS",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "AXA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "BANORTE",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "GENERAL DE SEGUROS",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "GNP",
                heightImg: '200px',
                widthImg: '450px',
            },
            {
                alias: "HDI",
                heightImg: '200px',
                widthImg: '450px',
            },
            {
                alias: "INBURSA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "MAPFRE",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "MIGO",
                heightImg: '150px',
                widthImg: '350px'
            },
            {
                alias: "EL POTOSI",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "QUALITAS",
                heightImg: '180px',
                widthImg: '450px'
            },
            {
                alias: "ZURA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "ZURICH",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "EL AGUILA",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "AIG",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "LA LATINO",
                heightImg: '200px',
                widthImg: '450px'
            },
            {
                alias: "Atlas Seguros",
                heightImg: '300px',
                widthImg: '300px'
            },
            {
                alias: "Bx+",
                heightImg: '200px',
                widthImg: '300px'
            }
        ];

        if (typeSize === 'w') {
            return sizes.find((a) => a.alias === alias).widthImg
        } else if (typeSize === 'h') {
            return sizes.find((a) => a.alias === alias).heightImg
        }
    }

}