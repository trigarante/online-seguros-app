import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';

import { ApplicationSettings, isAndroid, isIOS, knownFolders, path, Utils } from '@nativescript/core';
import { getFile } from '@nativescript/core/http';

import * as Permissions from 'nativescript-permissions';
import { PoliciesService } from '~/app/core/Services/Policies/policies.service';
import { environment } from '~/environment/environment';

declare const android: any;

@Component({
    selector: 'app-view',
    templateUrl: 'view.component.html',
    styleUrls: ['view.component.scss']
})

export class ViewComponent implements OnInit {

    titleActionBar: string = 'Mi Póliza';
    PDF_URL: string;
    PDFComplete: boolean = false;
    isBusy = true;
    registerPoliceID: number;
    filePolicie: string;
    policieNumberSave: string;

    textLoader = 'Cargandoo...';

    dowloadingText = 'Descargando su póliza...';
    isDownloading = false;

    isIOS: any;
    isAndroid: any;

    constructor(
        private activateRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private policiesService: PoliciesService
    ) {
        const params = JSON.parse(this.activateRoute.snapshot.params.file);
        this.filePolicie = params['file'];
        this.PDF_URL = `${environment.apiMark}/auth-cliente/poliza/${params['file']}`;
        this.titleActionBar = `Póliza No.: ${params['policieNumber']}`;
        this.policieNumberSave = params['policieNumber'];
        this.registerPoliceID = parseInt(params['registerID']);
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
    }

    ngOnInit() { }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onLoad() {
        this.PDFComplete = true;
    }

    downLoadPolicie() {
        this.isDownloading = true;
        const clientID: number = ApplicationSettings.getNumber('userActive');
        if (this.isAndroid === true) {
            Permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .then((response) => {
                    this.fileResponsePoliciePdf(this.filePolicie, clientID, this.registerPoliceID);
                })
                .catch((error) => {
                    console.error(error);
                });
        } else if (this.isIOS === true) {
            const currentDate = new Date().toISOString().split('T');
            const fileName = `Poliza-Num-${this.policieNumberSave}-[${currentDate[0]}]-[${currentDate[1]}].pdf`;
            const filePath = path.join(knownFolders.ios.downloads().path, fileName);

            getFile(this.PDF_URL, filePath)
                .then((resultFile) => {
                    alert({
                        title: 'Guardado',
                        okButtonText: 'Aceptar',
                        message: `Su póliza se guardó en:\n/Descargas/${fileName}`
                    });
                }, (error) => {
                    alert({
                        title: 'Error',
                        okButtonText: 'Aceptar',
                        message: 'No se pudo descargar su póliza'
                    });
                })
                .catch((error) => { console.error('Catch Download PDF => ', error); })
                .finally(() => {
                    this.isDownloading = false;
                    this.goBack();
                });
        }
    }

    fileResponsePoliciePdf(filePolicie: string, clientID: number, registerPoliceID: number) {
        const brandID: number = 4;
        const headers = { 'idcliente': clientID.toString(), 'idregistro': registerPoliceID.toString(), 'idmarca': brandID.toString() };
        this.policiesService.downLoadPolicie(filePolicie, headers).subscribe({
            next: (response) => { },
            error: (error) => { console.error('Error al descargar la póliza => ', error); },
            complete: async () => {
                const currentDate = new Date().toISOString().split('T');
                const fileName = `Poliza-Num-${this.policieNumberSave}-[${currentDate[0]}]-[${currentDate[1]}].pdf`;
                let downloadedFilePath = path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).getAbsolutePath(), fileName);

                await getFile(this.PDF_URL, downloadedFilePath)
                    .then((resultFile) => {
                        alert({
                            title: 'Guardado',
                            okButtonText: 'Aceptar',
                            message: `Su póliza se guardó en:\n/Descargas/${fileName}`
                        });
                    }, (error) => {
                        alert({
                            title: 'Error',
                            okButtonText: 'Aceptar',
                            message: 'No se pudo descargar su póliza'
                        });
                    })
                    .catch((error) => { console.error('Catch Download PDF => ', error); })
                    .finally(() => {
                        this.isDownloading = false;
                        this.goBack();
                    });
            }
        });
    }
}