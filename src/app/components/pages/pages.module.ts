import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { PrivacityComponent } from '~/app/shared/components/privacity/privacity.component';
import { SharedModule } from '~/app/shared/shared.module';
import { HomeComponent } from './home/components/home/home.component';
import { PagesRoutingModule } from './pages-router.module';
import { SettingsComponent } from './settings/components/settings/settings.component';
import { SupportComponent } from './support/components/support/support.component';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PagesRoutingModule,
        SharedModule
    ],
    declarations: [
        SupportComponent,
        SettingsComponent,
        HomeComponent
    ],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
