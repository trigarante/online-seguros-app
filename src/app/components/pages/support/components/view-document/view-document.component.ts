import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';
import { alert, isAndroid, isIOS } from '@nativescript/core';

import * as fileSystem from '@nativescript/core/file-system';

@Component({
    selector: 'app-view-document',
    templateUrl: 'view-document.component.html',
    styleUrls: ['view-document.component.scss']
})

export class ViewDocumentComponent implements OnInit {

    titleActionBar: string = '';

    documentName: string = '';
    folderName: string = '';
    PDF_URL: string = '';

    isIOS: boolean;
    isAndroid: boolean;

    alertOptions = {
        title: 'Documento no encontrado',
        message: 'Lamentamos los inconvenientes.\nLa aseguradora está renovando este documento',
        okButtonText: 'Aceptar',
    };

    constructor(
        private routerExtensions: RouterExtensions,
        private activatedRoute: ActivatedRoute
    ) {
        const params = JSON.parse(this.activatedRoute.snapshot.params.documentname);

        this.documentName = params.document;
        this.folderName = params.folder;
        this.titleActionBar = params.title;
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;

    }

    ngOnInit() {
        this.viewPDF();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    viewPDF() {
        if (this.documentName !== '') {
            const currentAppFolder = fileSystem.knownFolders.currentApp();
            const filePath = fileSystem.path.join(currentAppFolder.path, 'assets', 'docs', this.folderName, this.documentName);
            this.PDF_URL = filePath;
        } else {
            alert(this.alertOptions)
                .then(() => {
                    this.goBack();
                });
        }
    }

    onLoad() {
        console.log('PDF Cargado correctamente');
    }

}