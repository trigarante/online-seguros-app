import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { EventOfClaimComponent } from './components/event-of-claim.component';

const routes: Routes = [
    {
        path: '',
        component: EventOfClaimComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class EventOfClaimRoutingModule { }
