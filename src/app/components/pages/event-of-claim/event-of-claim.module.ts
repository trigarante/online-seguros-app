import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { EventOfClaimComponent } from './components/event-of-claim.component';

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    declarations: [
        EventOfClaimComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class EventOfClaimModule { }
