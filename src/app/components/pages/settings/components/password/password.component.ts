import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, isAndroid, isIOS, prompt, alert } from '@nativescript/core';
import {
    PromptResult,
    PromptOptions,
    inputType,
    capitalizationType,
} from '@nativescript/core/ui/dialogs/dialogs-common';
import { LoginModel } from '~/app/core/Models/Login/Login@Model';
import { AuthService } from '~/app/core/Services/Auth/Auth.service';


@Component({
    selector: 'app-security-password',
    templateUrl: 'password.component.html',
    styleUrls: ['password.component.scss']
})

export class SecurityPasswordComponent implements OnInit {

    titleActionBar: string = 'Cambiar Contraseña';

    isIOS: boolean;
    isAndroid: boolean;
    formGroup: FormGroup;

    confirmPass: boolean;
    userActive: number;
    isShowingPassword = false;

    constructor(
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private authService: AuthService
    ) {
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
        this.userActive = ApplicationSettings.getNumber('userActive');
    }

    goBack() {
        this.routerExtensions.backToPreviousPage()
    }

    ngOnInit() {
        this.startBuildForm();
    }

    confirmPassword() {
        let promptOptions: PromptOptions = {
            title: "Confirmación",
            message: "Escribe tu contraseña actual",
            okButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            cancelable: true,
            inputType: inputType.password, // email, number, text, password, or email
            capitalizationType: capitalizationType.sentences,
            // all. none, sentences or words
        };

        if (this.formGroup.valid) {

            const alertSuccessOptions = {
                title: 'Contraseña Actualizada',
                message: 'La contraseña ha sido cambiada.',
                okButtonText: 'Aceptar'
            };

            const alertPasswordDiffOptions = {
                title: 'Error de Confirmación',
                message: 'Contraseña Incorrecta',
                okButtonText: 'Aceptar'
            };

            const alertErrorOptions = {
                title: 'Error al actualizar',
                message: 'No se ha podido actualizar la contraseña',
                okButtonText: 'Aceptar'
            };

            if (this.passwordField.value === this.confirmPasswordField.value) {
                this.confirmPass = true;
                prompt(promptOptions)
                    .then((result: PromptResult) => {
                        if (result.result) {
                            const uID = ApplicationSettings.getNumber('userActive');
                            this.authService.updatePassword(result.text, this.confirmPasswordField.value, uID).subscribe({
                                next: (response) => {
                                    console.log('Response Update Password => ', response.status);
                                },
                                error: (error) => {
                                    console.error('Error Update Password => ', error.status);
                                    if (error.status === 400) {
                                        alert(alertPasswordDiffOptions);
                                    } else {
                                        alert(alertErrorOptions);
                                    }
                                },
                                complete: () => {
                                    console.log('Se actualizó correctamente');
                                    alert(alertSuccessOptions).then(() => { this.goBack(); });
                                }
                            });
                        }
                    });
            } else {
                this.confirmPass = false;
            }
        }
    }

    // Método para mostrar u ocultar la contraseña
    showPassword() {
        if (this.isShowingPassword) {
            this.isShowingPassword = false;
        } else {
            this.isShowingPassword = true;
        }
    }

    private startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            password: ["", [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'), Validators.minLength(8), Validators.maxLength(32)]],
            confirmPassword: ["", [Validators.required]]
        });
    }

    get passwordField(): AbstractControl {
        return this.formGroup.get("password");
    }

    get confirmPasswordField(): AbstractControl {
        return this.formGroup.get("confirmPassword");
    }


}