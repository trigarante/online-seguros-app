import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, Page } from '@nativescript/core';
import { ValidateSessionClass } from '~/app/core/Classes/ValidateSession/ValidateSession@Class';
import { UserModel } from '~/app/core/Models/User/User@Model';
import { AuthService } from '~/app/core/Services/Auth/Auth.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {

    isLogin: boolean = false;
    textLoader: string = "Iniciando Sesión";
    formGroup: FormGroup;
    userModel: UserModel;
    // Instancia de la clase que valida la sesión y retorna la url correspondiente
    validateSession: ValidateSessionClass = new ValidateSessionClass();
    url: string;
    isShowingPassword = false;

    constructor(
        private page: Page,
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private authService: AuthService
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.startBuildForm();
    }

    logIn(): void {
        this.isLogin = true;
        const alertOptions = {
            title: "Iniciar sesión",
            message: "Usuario y/o contraseña incorrectos.\nVerifique sus datos e intente de nuevo.",
            okButtonText: "Aceptar"
        };

        if (this.formGroup.valid) {
            /* Se envía como parámetro el formulario ya que este tiene los mismos nombres
            de la interfaz y hacen match automaticamente sin necesidad de crear un nuevo array. */
            this.authService.logIn(this.formGroup.value)
                // tslint:disable-next-line: deprecation
                .subscribe(
                    (data) => {
                        this.userModel = data;
                        ApplicationSettings.setNumber("userActive", Number(this.userModel.id));
                        ApplicationSettings.setNumber("userType", Number(this.userModel.tipo));
                        this.url = this.validateSession.validateSession(data);
                    },
                    (error) => {
                        // El error se usa para indicar al usuario que sus datos son incorrectos
                        if (error.status === 500) {
                            alert(alertOptions);
                        }
                        this.isLogin = false;
                    },
                    () => {
                        this.isLogin = false;
                        ApplicationSettings.setString('userPassword', this.passwordField.value);
                        this.redirectTo();
                    }
                );
        }
    }

    // Método para mostrar u ocultar la contraseña
    showPassword() {
        if (this.isShowingPassword) {
            this.isShowingPassword = false;
        } else {
            this.isShowingPassword = true;
        }
    }

    cotizar() {
        this.url = '/cotizador';
        this.redirectTo()
    }

    // Método para reedireccionar a la url correspondiente
    private redirectTo(): void {
        this.routerExtensions.navigate([this.url], {
            transition: {
                name: "fade"
            },
            clearHistory: true
        });
    }

    /* Método que construye el formulario de login con Formularios Reactivos de angular, el cual simplifica
    realizar validaciones y obtener los valores de los texfield */
    private startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            email: ["", [Validators.required, Validators.pattern("^[^@]+@[^@]+\.[a-zA-Z]{2,}$")]],
            contrasenaApp: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(32)]],
            idMarcaEmpresa: [4]
        });
    }

    /* Propiedades get para acceder a las propiedades del formGroup de una manera más sencilla
    Estas propiedades para este caso se usan para obtener los errores existentes
    y mostrarlos al usuario en el pantalla */
    get emailField(): AbstractControl {
        return this.formGroup.get("email");
    }

    get passwordField(): AbstractControl {
        return this.formGroup.get("contrasenaApp");
    }

    // Método para recuperar la contraseña
    forgotPassword() {
        this.url = '/recovery-password';
        this.redirectTo();
    }
}
