import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptCommonModule, NativeScriptFormsModule } from '@nativescript/angular';
import { PasswordRecoveryComponent } from './components/password-recovery.component';
import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';



@NgModule({
    imports: [
        NativeScriptCommonModule,
        PasswordRecoveryRoutingModule,
        ReactiveFormsModule,
        NativeScriptFormsModule
    ],
    declarations: [
        PasswordRecoveryComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PasswordRecoveryModule { }
