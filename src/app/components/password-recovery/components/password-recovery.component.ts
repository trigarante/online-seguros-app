import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS } from '@nativescript/core';
import { PasswordRecoveryService } from '~/app/core/Services/PasswordRecovery/passwordrecovery.service';

@Component({
    selector: 'app-password-recovery',
    templateUrl: 'password-recovery.component.html',
    styleUrls: ['password-recovery.component.scss']
})

export class PasswordRecoveryComponent implements OnInit {

    titleActionBar: string = 'Restaurar contraseña';
    isAndroid: boolean;
    isIOS: boolean;

    continue = false;

    formGroup: FormGroup;

    emailSent: boolean = false;

    constructor(
        private routerExtension: RouterExtensions,
        private formBuilder: FormBuilder,
        private recoveryService: PasswordRecoveryService
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() {
        this.startFormBuild();
    }

    goBack() {
        this.routerExtension.navigate(['/login'], {
            clearHistory: true,
            transition: {
                name: 'fade'
            }
        });
    }

    emailVerification() {
        const email = this.currentEmailField.value;
        let responseService = null;
        this.recoveryService.emailVerificationExist(email).subscribe({
            next: (response) => { responseService = response; },
            error: (error) => { console.error('Ah ocurrido un error => ', error); },
            complete: () => {
                if (responseService != null) {
                    const clientID = responseService.id;
                    this.sendEmail(clientID);
                } else {
                    alert({
                        title: 'Correo no registrado',
                        okButtonText: 'Aceptar',
                        message: 'El correo ingresado no existe en nuestros registros.'
                    });
                }
            }
        });
    }

    sendEmail(clientID: number) {
        this.recoveryService.sendEmailAfterVerification(clientID).subscribe({
            next: (data) => { },
            error: (error) => {
                alert({
                    title: 'Error al enviar correo',
                    okButtonText: 'Aceptar',
                    message: 'A ocurrido un error al tratar de enviar el correo electrónico.'
                });
                console.error('Error Send Email => ', error);
            },
            complete: () => {
                this.emailSent = true;
            }
        });
    }

    private startFormBuild(): void {
        this.formGroup = this.formBuilder.group({
            currentEmail: ["", [Validators.required, Validators.pattern("^[^@]+@[^@]+\.[a-zA-Z]{2,}$")]]
        });
    }

    get currentEmailField(): AbstractControl {
        return this.formGroup.get('currentEmail');
    }
}