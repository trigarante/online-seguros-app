import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { LogoutComponent } from './components/logout.component';

const routes: Routes = [
    {
        path: '',
        component: LogoutComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LogoutRoutingModule { }
